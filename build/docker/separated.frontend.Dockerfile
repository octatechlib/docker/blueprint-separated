FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Amsterdam

RUN mkdir -p /var/www/html/
RUN chmod 777 /var/www/html/

WORKDIR /var/www/html/

ARG PHP_CLI_DIR="/etc/php/7.4/cli"
ARG PHP_FPM_DIR="/etc/php/7.4/fpm"

RUN apt update
RUN apt install -y \
    apt-transport-https \
    apt-utils \
    ca-certificates \
    cron \
    curl \
    fontconfig \
    ghostscript \
    htop \
    libfontenc1 \
    locales \
    lsb-release \
    net-tools \
    nginx \
    poppler-utils \
    redis-server \
    software-properties-common \
    supervisor \
    vim \
    wget \
    x11-common \
    xfonts-75dpi \
    xfonts-base \
    xfonts-encodings \
    xfonts-utils

RUN locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

RUN apt-add-repository ppa:ondrej/php
RUN apt-get update

RUN apt-get install -y \
    php7.4-fpm \
    php7.4-bcmath \
    php7.4-common \
    php7.4-curl \
    php7.4-gd \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-mcrypt \
    php7.4-mysql \
    php7.4-opcache \
    php7.4-readline \
    php7.4-soap \
    php7.4-uuid \
    php7.4-xml \
    php7.4-xsl \
    php7.4-zip \
    php7.4-xdebug


# nginx
RUN mv /etc/nginx/sites-enabled /etc/nginx/sites-enabled_default
RUN mv /etc/nginx/nginx.conf /etc/nginx/nginx_default.conf

COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/nginx/sites-enabled/frontend.octa.local.conf /etc/nginx/sites-enabled/

EXPOSE 5000/tcp

CMD /etc/init.d/php7.4-fpm start -F && service cron start && service redis-server start && service supervisor start && nginx -g "daemon off;"
