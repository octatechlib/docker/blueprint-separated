FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Amsterdam

RUN mkdir -p /var/www/html/
RUN chmod 777 /var/www/html/

WORKDIR /var/www/html/

ARG PHP_CLI_DIR="/etc/php/7.4/cli"
ARG PHP_FPM_DIR="/etc/php/7.4/fpm"

RUN apt update
RUN apt install -y \
    apt-transport-https \
    apt-utils \
    ca-certificates \
    cron \
    curl \
    fontconfig \
    ghostscript \
    htop \
    libfontenc1 \
    locales \
    lsb-release \
    net-tools \
    nginx \
    poppler-utils \
    redis-server \
    software-properties-common \
    supervisor \
    vim \
    wget \
    x11-common \
    xfonts-75dpi \
    xfonts-base \
    xfonts-encodings \
    xfonts-utils

RUN locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

RUN apt-add-repository ppa:ondrej/php
RUN apt-get update

RUN apt-get install -y \
    php7.4-fpm \
    php7.4-bcmath \
    php7.4-common \
    php7.4-curl \
    php7.4-gd \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-mcrypt \
    php7.4-mysql \
    php7.4-opcache \
    php7.4-readline \
    php7.4-soap \
    php7.4-uuid \
    php7.4-xml \
    php7.4-xsl \
    php7.4-zip \
    php7.4-xdebug

RUN mv /etc/php/7.4/fpm/php.ini /etc/php/7.4/fpm/php_default.ini

# redis
RUN mv /etc/redis/redis.conf /etc/redis/redis_default.conf
COPY ./docker/redis/redis.conf /etc/redis/redis.conf

# supervisor
RUN rm -f /etc/supervisor/conf.d/*
COPY ./docker/supervisor/* /etc/supervisor/conf.d/
COPY ./docker/supervisord.conf /etc/supervisord.conf

# cron
COPY ./docker/cron/* /etc/cron.d
RUN mkdir -p /var/log/octa-cron/ && \
    touch /var/log/octa-cron/cron-backend-schedule-docker.log && \    
    chmod 777 /var/log/octa-cron/ && \
    find /var/log/octa-cron/ -type f -exec chmod 666 {} + 
    # && \
    #find /var/log/octa-cron/ -type f -exec chown www-data.www-data {} +

# deb
RUN mkdir -p /tmp/deb
RUN chmod 777 /tmp/deb

COPY ./docker/deb/* /tmp/deb/

RUN rm -rf /tmp/deb

# ssl
RUN mv /etc/ssl/openssl.cnf /etc/ssl/openssl_default.cnf
COPY ./docker/ssl/openssl.conf /etc/ssl/openssl.cnf
RUN update-ca-certificates

# xdebug
RUN mkdir /var/log/xdebug/ && \
    touch /var/log/xdebug/xdebug.log && \
    chmod 777 /var/log/xdebug/ && \
    chmod 666 /var/log/xdebug/xdebug.log

COPY ./docker/xdebug/xdebug.ini /etc/php/7.4/mods-available/xdebug.ini
COPY ./docker/xdebug/xdebug.ini /etc/php/7.4/cli/conf.d/20-xdebug.ini

# nginx
RUN mv /etc/nginx/sites-enabled /etc/nginx/sites-enabled_default
RUN mv /etc/nginx/nginx.conf /etc/nginx/nginx_default.conf

COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/nginx/sites-enabled/backend.octa.local.conf /etc/nginx/sites-enabled/

# php
COPY ./docker/php/tweaks.ini /usr/local/etc/php/conf.d/tweaks.ini

EXPOSE 80/tcp

CMD /etc/init.d/php7.4-fpm start -F && service cron start && service redis-server start && service supervisor start && nginx -g "daemon off;"
