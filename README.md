# Blueprint Separated

Separated per type config skeleton for bi-container based structure.

## Getting started

This is a blueprint for basic docker based configuration. Two services separated per own container. PHP version specified in Dockerfile. Adjust to your needs. Pay attention to nginx configs.

#### Must know
Meant for MacOS. Other ymls soon.

## License
[![License](https://img.shields.io/badge/License-GPL_2.0-blue.svg)](https://opensource.org/license/gpl-2-0/)

## Project status
In progress.
