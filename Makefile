help:	     ## Show this help
	@echo ""
	@echo "Usage:  make COMMAND"
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

build:	     ## (Re)build locally the docker containers for this application
	docker-compose -f $(PWD)/build/docker/docker-compose.yml --profile=all build
.PHONY: build

up:          ## Put up the docker containers for local development
	docker-compose -f $(PWD)/build/docker/docker-compose.yml --profile=all up -d
.PHONY: up

down:	     ## Put down and remove the docker containers for local development
	docker-compose -f $(PWD)/build/docker/docker-compose.yml down
.PHONY: down

start:	     ## Start the docker containers for local development
	docker-compose -f $(PWD)/build/docker/docker-compose.yml start
.PHONY: stop

stop:	     ## Stop the docker containers for local development
	docker-compose -f $(PWD)/build/docker/docker-compose.yml stop
.PHONY: stop

tail:	     ## Tail the log files of the containers
	docker-compose -f $(PWD)/build/docker/docker-compose.yml logs -f -t --tail=20
.PHONY: tail

in-fe:          ## Go inside of main container
	docker exec -it separated_service_frontend /bin/bash
.PHONY: in-fe

in-be:          ## Go inside of main container
	docker exec -it separated_service_backend /bin/bash
.PHONY: in-be

reload:      ## Go down, build, compose up and go inside
	docker-compose -f $(PWD)/build/docker/docker-compose.yml down && docker compose -f $(PWD)/build/docker/docker-compose.yml build && docker compose -f $(PWD)/build/docker/docker-compose.yml --profile=all up -d && docker exec -it docker-standalone-1 /bin/bash
.PHONY: reload

reset:       ## Go down, DESTROY, build, compose up and go inside
	docker-compose -f $(PWD)/build/docker/docker-compose.yml down && docker system prune --all && docker compose -f $(PWD)/build/docker/docker-compose.yml build && docker compose -f $(PWD)/build/docker/docker-compose.yml --profile=all up -d && docker exec -it docker-standalone-1 /bin/bash
.PHONY: reset

remove:      ## Go down, DESTROY and that's it
	docker-compose -f $(PWD)/build/docker/docker-compose.yml down && docker system prune --all -f
.PHONY: remove

remove-hard: ## Go down, DESTROY alllllll the volumes
	docker volume rm $(docker volume ls -q)
.PHONY: remove-hard